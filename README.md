# BEC VSCode Extension

This is an extension for better integration into the Beamline-Experiment-Control (BEC) software.

## Features

It is an early version of the extension and is not meant to be used in production.

## Known Issues

- The extension is not yet fully functional

## Release Notes


---


## Build and upload the extension
To build the extension, run the following command in the terminal:
```bash
vsce package
```

Log in to the [marketplace website](https://marketplace.visualstudio.com/manage/publishers/beamline-experiment-control) and upload the generated `.vsix` file.


