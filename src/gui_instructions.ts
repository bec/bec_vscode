import * as vscode from "vscode";

interface GuiInstructionMessage {
  command:
    | "open"
    | "write"
    | "close"
    | "zenMode"
    | "save"
    | "new"
    | "setCursor";
  content: string;
}

export function onGuiInstruction(message: string, channel: string) {
  // handler for GUI instructions sent over Redis
  console.log("Received GUI Instruction:", message);
  let message_content: GuiInstructionMessage;
  try {
    message_content = JSON.parse(message);
  } catch (e) {
    console.error("Error processing GUI instruction:", e);
    return;
  }

  if (message_content.command === "open") {
    vscode.commands.executeCommand(
      "vscode.open",
      vscode.Uri.parse(message_content.content)
    );
    return;
  }
  if (message_content.command === "zenMode") {
    vscode.commands.executeCommand("workbench.action.toggleZenMode");
    return;
  }
  if (message_content.command === "write") {
    // Write the content to the current active editor and the cursor position
    const editor = vscode.window.activeTextEditor;
    if (editor) {
      editor.edit((editBuilder) => {
        editBuilder.insert(editor.selection.active, message_content.content);
      });
    }
  }
  if (message_content.command === "close") {
    const editor = vscode.window.activeTextEditor;
    if (editor) {
      vscode.commands.executeCommand("workbench.action.closeActiveEditor");
    }
  }
  if (message_content.command === "save") {
    const editor = vscode.window.activeTextEditor;
    if (editor) {
      editor.document.save();
    }
  }
  if (message_content.command === "new") {
    vscode.commands.executeCommand("workbench.action.files.newUntitledFile");
  }
  if (message_content.command === "setCursor") {
    // parse the line number and column number from the message content
    let line_number = 0;
    let column_number = 0;

    const parts = message_content.content.split(",");
    if (parts.length === 2) {
      line_number = parseInt(parts[0]);
      column_number = parseInt(parts[1]);
    }

    const editor = vscode.window.activeTextEditor;
    if (editor) {
      // Set the cursor position if the given line number and column number are valid
      // and within the bounds of the current document. Otherwise set the cursor to the end of the document.
      const newPosition = new vscode.Position(
        Math.min(
          line_number,
          vscode.window.activeTextEditor?.document.lineCount || 0
        ),
        Math.min(
          column_number,
          vscode.window.activeTextEditor?.document.lineAt(line_number).text
            .length || 0
        )
      );
      const newSelection = new vscode.Selection(newPosition, newPosition);
      editor.selection = newSelection;
    }
  }
}
