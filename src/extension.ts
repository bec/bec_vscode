import * as vscode from "vscode";
import * as redis from "redis";
import { onGuiInstruction } from "./gui_instructions";

let client: redis.RedisClientType | null = null;
let subscriber: redis.RedisClientType | null = null;

function getGuiId() {
  const guiId = process.env["BEC_Widgets_GUIID"];
  if (guiId) {
    return guiId;
  }
  return null;
}

function getRedisUrl() {
  let redisHost = process.env["BEC_REDIS_HOST"];
  if (redisHost) {
    return `redis://${redisHost}:6379`;
  }
  redisHost = vscode.workspace
    .getConfiguration("bec")
    .get<string>("host", "localhost");
  return `redis://${redisHost}:6379`;
}

async function createRedisClient(url: string) {
  if (client) {
    client.quit(); // Clean up any existing Redis connection
  }
  client = redis.createClient({ url: url });

  client.on("error", (err: Error) => {
    console.error("Redis Client Error", err);
  });

  client.on("connect", () => {
    console.log("Connected to Redis on ", url);
  });

  await client.connect();
  subscriber = client?.duplicate();
  await subscriber.connect();
  subscriber?.subscribe("vscode-instructions/" + getGuiId(), onGuiInstruction);
}

export async function activate(context: vscode.ExtensionContext) {
  if (!getGuiId()) {
    console.info("BEC_Widgets_GUIID environment variable not set");
    return;
  }

  // Initial configuration
  const url = getRedisUrl();

  const config = vscode.workspace.getConfiguration("zenMode");
  config.update("fullScreen", false, vscode.ConfigurationTarget.Global);
  config.update("centerLayout", false, vscode.ConfigurationTarget.Global);

  // Create the initial Redis client
  await createRedisClient(url);

  // Listen for configuration changes
  vscode.workspace.onDidChangeConfiguration(async (event) => {
    if (event.affectsConfiguration("bec.redisHost")) {
      const newRedisHost = vscode.workspace
        .getConfiguration("bec")
        .get<string>("host", "localhost");
      const url = `redis://${newRedisHost}:6379`;
      console.log("Redis host configuration changed to:", url);

      // Recreate the Redis client with the new host
      await createRedisClient(url);
    }
  });

  vscode.workspace.onDidSaveTextDocument((event: vscode.TextDocument) => {
    if (client) {
      const content = JSON.stringify({
        uri: event.uri.toString(),
        content: event.getText(),
      });
      const guiId = getGuiId();
      console.log("Text Document Saved:", content);
      client.publish(`vscode-events/${guiId}`, content);
    }
  });
  console.log('Congratulations, your extension "bec" is now active!');
}

// This method is called when your extension is deactivated
export function deactivate() {
  if (client) {
    client.quit(); // Ensure Redis connection is properly closed when the extension is deactivated
  }
}
